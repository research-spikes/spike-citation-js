
$(function(){
    console.log('loaded');
    //var Cite = require('citation-js');

    function getInput(){
        return $('.input').val();
    }

    function getOutputOptions(){
        return {
            format: $('.output-format').val(),
            type: $('.output-type').val(),
            style: $('.output-style').val(),
            lang: 'en_US'
        };
    }

    //https://citation.js.org/api/tutorial-output_options.html
    function parse(input){
        /*
            format: real (json),| string
            type: json\html\string
            style: csl (json)|bibtex\bibtext\citation-<template>
        */
        var inputOptions = {
            output : getOutputOptions()
        };
        var c = new Cite(input, inputOptions);
        console.log(c);
        return c.get();
    }

    $('.btn-test').click(function(){
        var input = getInput();
        var output = parse(input);
        if(_.isString(output)){
            $('.output').val(output);
        }else{
            $('.output').val(JSON.stringify(output));
        }
    });

    var testBibJSON = {
        "source": "PubMed",
        "accessed": {
            "date-parts": [
                [
                    2018,
                    11,
                    15
                ]
            ]
        },
        "id": "pmid:27375235",
        "title": "A chaotic self-oscillating sunlight-driven polymer actuator",
        "author": [
            {
                "family": "Kumar",
                "given": "Kamlesh"
            },
            {
                "family": "Knie",
                "given": "Christopher"
            },
            {
                "family": "Bléger",
                "given": "David"
            },
            {
                "family": "Peletier",
                "given": "Mark A"
            },
            {
                "family": "Friedrich",
                "given": "Heiner"
            },
            {
                "family": "Hecht",
                "given": "Stefan"
            },
            {
                "family": "Broer",
                "given": "Dirk J"
            },
            {
                "family": "Debije",
                "given": "Michael G"
            },
            {
                "family": "Schenning",
                "given": "Albertus P H J"
            }
        ],
        "container-title-short": "Nat Commun",
        "container-title": "Nature communications",
        "publisher": "Nature Publishing Group",
        "ISSN": "2041-1723",
        "issued": {
            "date-parts": [
                [
                    2016,
                    7,
                    4
                ]
            ]
        },
        "page": "11975",
        "volume": "7",
        "PMID": "27375235",
        "PMCID": "PMC4932179",
        "DOI": "10.1038/ncomms11975",
        "type": "article-journal"
    };

    $('.input').val(JSON.stringify(testBibJSON));

});
