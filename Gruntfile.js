module.exports = function(grunt){
    'use strict';

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        clean: {
            libs : ['js/libs'],
            css : ['css'],
        },

        jshint: { //TODO: review
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                node: true,
                globals: {
                    $: true,
                    _: true,
                    xit: true,
                    //document
                    window: false,
                    document: false,
                    // require.js
                    define: false,
                    // plug-ins
                    LocalFileSystem : false,
                    // mocha testing
                    mochaPhantomJS : false,
                    // citation-js library
                    Cite: true
                }
            },
            src: 'js/index.js'
        },

        compass: {
            dev: {
                options :{
                    sassDir: 'sass',
                    cssDir: 'css',
                    outputStyle: 'expanded',
                    relativeAssets: true,
                    imagesDir: 'img',
                    fontsDir: 'fonts'
                }
            }
        },

        watch: {
            css : {
                files: ['sass/**/*.scss'],
                tasks: ['compass:dev']
            }
        },
        serve : {
            options : {
                port : 8000
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-serve');

    grunt.registerTask('build', [
        'clean:css',
        'jshint',
        'compass:dev'
    ]);
};
